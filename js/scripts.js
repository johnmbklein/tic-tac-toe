
//game object
var game = {
  board: [[0,0,0],[0,0,0],[0,0,0]],
  turn: 0,
  winner: 0,
  activePlayer: 1
};

//player logic (constructor)
function Player(name) {
  this.name = name;
  this.wins = 0;
  this.isComputer = false;
};

Object.prototype.newGame = function (){
  this.board = [[0,0,0],[0,0,0],[0,0,0]];
  this.turn = 0;
  this.winner = 0;
  this.activePlayer = 1;
  game.drawBoard();
};

Object.prototype.drawBoard = function(){
  for (var j = 0; j < this.board.length; j++) {
    for (var i = 0; i < this.board[j].length; i++) {
      if (this.board[j][i] === 1) {
        $("#c"+j+i).text("X");
      } else if (this.board[j][i] === 2) {
        $("#c"+j+i).text("O");
      } else if (this.board[j][i] === 0) {
        $("#c"+j+i).text("");
      }
    }
  }
  $('#winCountOne').text(player1.wins);
  $('#winCountTwo').text(player2.wins);
};

Object.prototype.newTurn = function(){
  this.turn ++;
  if (this.activePlayer === 1) {
    this.activePlayer = 2;
  } else if (this.activePlayer === 2) {
    this.activePlayer = 1;
  }
};

Object.prototype.mark = function (x,y) {
  if (this.board[x][y] !== 0) {
    alert('error!');
  } else if (this.activePlayer === 1) {
      this.board[x][y] = 1;
  } else if (this.activePlayer === 2) {
      this.board[x][y] = 2;
  }
  game.drawBoard();
};

Object.prototype.checkVictory = function() {
  for (var j = 1; j < 3; j++) {

    //check horizontal victory
    for (var i = 0; i < this.board.length; i++) {
      if (this.board[0][i] === j && this.board[1][i] === j && this.board[2][i] === j) {
      alert("player " + j + " won horizontally in row " + i +"!");
      return j;
      }
    }

    //check vertical victory
    for (var i = 0; i < this.board.length; i++) {
      if (this.board[i][0] === j && this.board[i][1] === j && this.board[i][2] === j) {
      alert("player " + j + " won vertically in column " + i +"!");
      return j;
      }
    }

    // check diagonal victory
    if (this.board[0][0] === j && this.board[1][1] === j && this.board[2][2] === j) {
      alert("player " + j + " won diagonally!");
      return j;
    } else if (this.board[2][0] === j && this.board[1][1] === j && this.board[0][2] === j) {
      alert("player " + j + " won diagonally!");
      return j;
    }
  }

  // check draw
  if (this.turn === 8) {
    alert('draw');
    return 3;
  }
  return 0;

}

//Dumb computer player logic
var computerTurn = function() {
  do {
    var x = Math.floor(3*Math.random());
    var y = Math.floor(3*Math.random());
  } while (game.turn < 8 && game.board[x][y] !== 0);
  game.mark(x,y);
  var victory = game.checkVictory();
  if (victory === 0) {
    game.newTurn();
  } else if (victory === 2){
    player2.wins ++;
    game.newGame();
  }
};

//user interface logic
$('#grid').hide();
$('.playerWinCount').hide();
$("#playerSubmit").submit(function(event) {
  $('form').hide();
  $('.playerWinCount').show();
  $('#grid').show();
  event.preventDefault();
  var playerOneName = $("#player1").val();
  player1 = new Player(playerOneName);
  var playerTwoName = $("#player2").val();
  player2 = new Player(playerTwoName);
  $('#playerNameOne').text(player1.name + "\'s");
  $('#playerNameTwo').text(player2.name + "\'s");
  if ($("#checkBox:checked").val() === "on") {
    player2.isComputer = true;
  }
});

$(".square").click(function(){
  var coordinate = this.id.slice(1,3);
  var x = parseInt(coordinate.slice(0,1));
  var y = parseInt(coordinate.slice(1,2));
  if (game.board[x][y] === 0) {
    game.mark(x,y);
    var victory = game.checkVictory();
    if (victory === 0) {
      game.newTurn();
      if (player2.isComputer === true) {
        computerTurn();
      }
    }
    if (victory === 1){
      player1.wins ++;
      game.newGame();
    } else if (victory === 2){
      player2.wins ++;
      game.newGame();
    } else if (victory === 3){
      game.newGame();
    }
  } else {
    alert("click on an empty square");
  }
});
